<?php

namespace App\Http\Controllers;

class DirController extends Controller
{
    public function buscarArchivos()
    {
        $season = '18'; //Numero de la temporada

        $menos = 746; //Numero que le restara al capitulo actual

        $buscar = 24; //Numero de la linea que va a buscar en el text

        $prueba = request('prueba') != 'false';

        $ruta = "E:\Video\Anime\One Piece\Season {$season}";
        $directorio = opendir($ruta); //ruta actual
        $arr = [];
        while ($archivo = readdir($directorio)) { //obtenemos un archivo y luego otro sucesivamente
            if (!is_dir($archivo) && (strpos($archivo, '[PuyaSubs!]') !== false)) {
                $oldNum = substr($archivo, $buscar, 3);
                $tipo = substr($archivo, -3, 3);
                $newNum = str_pad((int)$oldNum - $menos, 2, "0", STR_PAD_LEFT);

                $nuevoNombre = "One Piece - S{$season}E{$newNum} [{$oldNum}].{$tipo}";

                if ($archivo != $nuevoNombre) {
                    $rutaArchivo1 = "{$ruta}\\{$archivo}";
                    $rutaArchivo2 = "{$ruta}\\{$nuevoNombre}";
                    if ($prueba) {
                        $arr[] = "Prueba: => El archivo {$archivo} se ha renombrado a {$nuevoNombre}";
                    } elseif (rename($rutaArchivo1, $rutaArchivo2)) {
                        $arr[] = "El archivo {$archivo} se ha renombrado a {$nuevoNombre}";
                    } else {
                        $arr[] = "El archivo {$archivo} no se ha renombrado correctamente";
                    }
                }
            }
        }

        return view('welcome', compact('arr', 'prueba'));
    }

    public function onePiece()
    {
        $season = '19'; //Numero de la temporada

        $menos = 779; //Numero que le restara al capitulo actual

        $buscar = 20; //Numero de la linea que va a buscar en el text

        $prueba = request('prueba') != 'false';

        $ruta = "E:\Video\Anime\One Piece\Season {$season}";
        $directorio = opendir($ruta); //ruta actual
        $arr = [];
        while ($archivo = readdir($directorio)) { //obtenemos un archivo y luego otro sucesivamente
            if (!is_dir($archivo) && (strpos($archivo, '[A-Kei]') !== false)) {
                $oldNum = substr($archivo, $buscar, 3);
                $tipo = substr($archivo, -3, 3);
                $newNum = str_pad((int)$oldNum - $menos, 2, "0", STR_PAD_LEFT);

                $nuevoNombre = "One Piece - S{$season}E{$newNum} [{$oldNum}].{$tipo}";

                if ($archivo != $nuevoNombre) {
                    $rutaArchivo1 = "{$ruta}\\{$archivo}";
                    $rutaArchivo2 = "{$ruta}\\{$nuevoNombre}";
                    if ($prueba) {
                        $arr[] = "Prueba: => El archivo {$archivo} se ha renombrado a {$nuevoNombre}";
                    } elseif (rename($rutaArchivo1, $rutaArchivo2)) {
                        $arr[] = "El archivo {$archivo} se ha renombrado a {$nuevoNombre}";
                    } else {
                        $arr[] = "El archivo {$archivo} no se ha renombrado correctamente";
                    }
                }
            }
        }

        return view('welcome', compact('arr', 'prueba'));
    }

    public function cambiarTipo()
    {
        $season = '19'; //Numero de la temporada
        $prueba = request('prueba') != 'false';

        $ruta = "E:\Video\Anime\One Piece\Season {$season}";
        $directorio = opendir($ruta); //ruta actual
        //dd($directorio);
        $arr = [];
        while ($archivo = readdir($directorio)) { //obtenemos un archivo y luego otro sucesivamente
            if (!is_dir($archivo)) {

                $nuevoNombre = str_replace('mkv', 'mp4', $archivo);

                if ($archivo != $nuevoNombre) {
                    $rutaArchivo1 = "{$ruta}\\{$archivo}";
                    $rutaArchivo2 = "{$ruta}\\{$nuevoNombre}";
                    if ($prueba) {
                        $arr[] = "Prueba: => El archivo {$archivo} se ha renombrado a {$nuevoNombre}";
                    } elseif (rename($rutaArchivo1, $rutaArchivo2)) {
                        $arr[] = "El archivo {$archivo} se ha renombrado a {$nuevoNombre}";
                    } else {
                        $arr[] = "El archivo {$archivo} no se ha renombrado correctamente";
                    }
                }
            }
        }

        return view('welcome', compact('arr', 'prueba'));
    }

    public function bleach()
    {
        $season = '02'; //Numero de la temporada

        $menos = 20; //Numero que le restara al capitulo actual

        $buscar = 17; //Numero de la linea que va a buscar en el text

        $prueba = request('prueba') != 'false';

        $ruta = "E:\Video\Anime\Bleach\Season {$season}";
        $directorio = opendir($ruta); //ruta actual
        $arr = [];
        while ($archivo = readdir($directorio)) { //obtenemos un archivo y luego otro sucesivamente
            if (!is_dir($archivo) && (strpos($archivo, '[A-Kei]') !== false)) {
                $oldNum = substr($archivo, $buscar, 2);
                $tipo = substr($archivo, -3, 3);
                $newNum = str_pad((int)$oldNum - $menos, 2, "0", STR_PAD_LEFT);

                $nuevoNombre = "Bleach - S{$season}E{$newNum} [{$oldNum}].{$tipo}";

                if ($archivo != $nuevoNombre) {
                    $rutaArchivo1 = "{$ruta}\\{$archivo}";
                    $rutaArchivo2 = "{$ruta}\\{$nuevoNombre}";
                    if ($prueba) {
                        $arr[] = "Prueba: => El archivo {$archivo} se ha renombrado a {$nuevoNombre}";
                    } elseif (rename($rutaArchivo1, $rutaArchivo2)) {
                        $arr[] = "El archivo {$archivo} se ha renombrado a {$nuevoNombre}";
                    } else {
                        $arr[] = "El archivo {$archivo} no se ha renombrado correctamente";
                    }
                }
            }
        }

        return view('welcome', compact('arr', 'prueba'));
    }
}
