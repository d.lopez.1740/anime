<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DirController@buscarArchivos');

Route::get('/tipo', 'DirController@cambiarTipo');

Route::get('/one-piece', 'DirController@onePiece');

Route::get('/bleach', 'DirController@bleach');
